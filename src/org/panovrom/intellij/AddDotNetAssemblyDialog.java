/**
 * Copyright (C) 2016, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.ui.ColoredListCellRenderer;
import com.intellij.ui.DocumentAdapter;
import com.intellij.ui.Gray;
import com.intellij.ui.JBColor;
import com.intellij.ui.SeparatorComponent;
import com.intellij.ui.SimpleTextAttributes;
import com.intellij.ui.components.JBList;
import com.intellij.ui.components.JBScrollPane;
import com.intellij.util.ui.UIUtil;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.LayoutManager;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.AbstractButton;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.DocumentEvent;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.JTextComponent;

final class AddDotNetAssemblyDialog extends JDialog {
  private static final int INITIAL_VIEW_SIZE = 5;
  private static final int VIEW_SIZE_INCREMENT = 15;
  private static final SimpleTextAttributes SEARCH_MATCH_ATTRIBUTES =
      new SimpleTextAttributes(SimpleTextAttributes.STYLE_SEARCH_MATCH, null);

  // Model content (hard-coded in files SystemAssemblies.txt and LocalAssemblies.txt).
  private static List<Assembly> ourRecentAssemblies;
  private static List<Assembly> ourSystemAssemblies;
  private static List<Assembly> ourLocalAssemblies;

  private final LayoutManager myLayoutManager;
  private final JTextComponent mySearchTextField;
  private final Component myBrowseButton;
  private final JList myList;
  private final DefaultListModel myListModel;
  private final JScrollPane myListScrollPane;
  private final PropertyPane myPropertyPane;
  private Dimension myDialogMinSize;
  private Dimension myContentMinSize;
  private String mySearchQuery;
  private Searcher mySearcher;
  private final Runnable myListUpdater;
  private final GroupData[] myGroupsData;
  private final Lock myFoundAssembliesLock;
  private final ListSelection myListSelection;
  private final Point myViewportPosition;
  private FileFilter myFileFilter;

  @SuppressWarnings("unchecked")
  AddDotNetAssemblyDialog(Frame owner) {
    super(owner, ".Net Assemblies", true);
    loadAssemblies();

    final LayoutManager layoutManager = new DialogLayoutManager();
    setLayout(layoutManager);

    final JTextComponent searchTextField = new JTextField(20);
    searchTextField.setFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS,
                                          Collections.EMPTY_SET);
    final SearchTextListener searchTextListener = new SearchTextListener();
    searchTextField.addKeyListener(searchTextListener);
    searchTextField.getDocument().addDocumentListener(searchTextListener);

    final AbstractButton browseButton = new JButton("Browse...");
    browseButton.addActionListener(new BrowseButtonListener());

    final JList list = new JBList();
    final DefaultListModel listModel = new DefaultListModel();
    final ListListener listListener = new ListListener();
    list.setModel(listModel);
    list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    list.setCellRenderer(new ListCellRenderer());
    list.addMouseListener(listListener);
    list.addFocusListener(listListener);
    list.addListSelectionListener(listListener);
    final JScrollPane listScrollPane = new JBScrollPane(list);

    final PropertyPane propertyPane = new PropertyPane();

    final Group[] groups = Group.class.getEnumConstants();
    assert groups != null;
    final GroupData[] groupsData = new GroupData[groups.length];

    for (final Group group : groups) {
      groupsData[group.ordinal()] = new GroupData(group);
    }

    add(searchTextField);
    add(browseButton);
    add(listScrollPane);
    add(propertyPane);
    addComponentListener(new DialogListener());

    myLayoutManager = layoutManager;
    mySearchTextField = searchTextField;
    myBrowseButton = browseButton;
    myList = list;
    myListModel = listModel;
    myListScrollPane = listScrollPane;
    myPropertyPane = propertyPane;
    myDialogMinSize = null;
    myContentMinSize = null;
    mySearchQuery = null;
    mySearcher = null;
    myListUpdater = new ListUpdater();
    myGroupsData = groupsData;
    myFoundAssembliesLock = new ReentrantLock();
    myListSelection = new ListSelection();
    myViewportPosition = new Point();
    myFileFilter = null;
  }

  @SuppressWarnings("unchecked")
  private void launchSearcher(String searchQuery) {
    if (mySearcher != null) {
      // Cancel a previously submitted search task.
      mySearcher.cancel();
    }

    mySearchQuery = searchQuery;
    mySearcher = new Searcher(searchQuery);
    ApplicationManager.getApplication().executeOnPooledThread(mySearcher);
  }

  @SuppressWarnings("unchecked")
  private void updateList() {
    final GroupData[] groupsData = myGroupsData;
    final Lock lock = myFoundAssembliesLock;
    final DefaultListModel listModel = myListModel;

    if (lock.tryLock()) {
      try {
        myListModel.clear();

        for (final GroupData groupData : groupsData) {
          final List<FoundAssembly> foundAssemblies = groupData.myFoundAssemblies;
          int count = groupData.myFoundAssembliesCount;
          final int viewSize = groupData.myViewSize;
          final TitleElement titleElement = groupData.myTitleElement;
          final MoreElement moreElement = groupData.myMoreElement;
          int firstIdx = -1;
          int lastIdx = -1;

          if (foundAssemblies != null && count > 0 && viewSize > 0) {
            listModel.addElement(titleElement);
            boolean toShowMore = false;

            if (count > viewSize) {
              count = viewSize;
              toShowMore = true;
            }

            firstIdx = listModel.size();
            lastIdx = firstIdx + count - 1;

            for (int i = 0; i < count; ++i) {
              listModel.addElement(foundAssemblies.get(i));
            }

            if (toShowMore) {
              listModel.addElement(moreElement);
            }
          }

          groupData.myFirstIndexInList = firstIdx;
          groupData.myLastIndexInList = lastIdx;
        }
      } finally {
        lock.unlock();
      }

      restoreListSelection();
    }
  }

  private void restoreListSelection() {
    final ListSelection listSelection = myListSelection;
    final JList list = myList;
    final DefaultListModel listModel = myListModel;
    final int listSize = listModel.size();

    if (listSelection.isValid()) {
      final GroupData groupData = myGroupsData[listSelection.myGroupIndex];
      final int indexInList = groupData.myFirstIndexInList + listSelection.myIndexInGroup;

      if (indexInList >= 0 && indexInList < listSize &&
          listModel.get(indexInList) instanceof FoundAssembly) {
        list.setSelectedIndex(indexInList);
      } else {
        listSelection.invalidate();
        list.clearSelection();
      }
    } else {
      if (!selectFirstAssembly()) {
        listSelection.invalidate();
        list.clearSelection();
      }
    }
  }

  private boolean selectFirstAssembly() {
    final JList list = myList;
    final DefaultListModel listModel = myListModel;
    final int listSize = myListModel.size();

    for (int idx = 0; idx < listSize; ++idx) {
      final Object element = listModel.get(idx);

      if (element instanceof FoundAssembly) {
        list.setSelectedIndex(idx);
        return true;
      }
    }

    return false;
  }

  private void selectNextAssembly() {
    final JList list = myList;
    final DefaultListModel listModel = myListModel;
    final int listSize = myListModel.size();
    final int selectedIdx = Math.max(list.getSelectedIndex(), 0);

    for (int idx = selectedIdx + 1; idx < listSize; ++idx) {
      final Object element = listModel.get(idx);

      if (element instanceof FoundAssembly) {
        scrollToCells(idx, idx, true);
        list.setSelectedIndex(idx);
        break;
      }
    }
  }

  private void selectPreviousAssembly() {
    final JList list = myList;
    final DefaultListModel listModel = myListModel;
    final int listSize = myListModel.size();
    final int selectedIdx = Math.min(list.getSelectedIndex(), listSize - 1);

    for (int idx = selectedIdx - 1; idx > 0; --idx) {
      final Object element = listModel.get(idx);

      if (element instanceof FoundAssembly) {
        scrollToCells(idx, idx, false);
        list.setSelectedIndex(idx);
        break;
      }
    }
  }

  private void selectNextGroup() {
    final int selectGroupIdx = myListSelection.myGroupIndex;

    if (selectGroupIdx < 0) {
      return;
    }

    final GroupData[] groupsData = myGroupsData;
    final int groupCount = groupsData.length;
    assert selectGroupIdx < groupCount;
    int nextGroupIdx = selectGroupIdx + 1;
    GroupData nextGroupData = null;
    boolean goDown = true;

    while (nextGroupIdx != selectGroupIdx) {
      if (nextGroupIdx >= groupCount) {
        nextGroupIdx = 0;
        goDown = false;
      }

      nextGroupData = groupsData[nextGroupIdx];

      if (nextGroupData.myFirstIndexInList < 0 ||
          nextGroupData.myLastIndexInList < 0) {
        nextGroupData = null;
      } else {
        break;
      }

      ++nextGroupIdx;
    }

    if (nextGroupData == null) {
      return;
    }

    final DefaultListModel listModel = myListModel;
    final int listSize = listModel.size();
    final int idxToSelect = nextGroupData.myFirstIndexInList;

    if (idxToSelect <= 0 || idxToSelect >= listSize) {
      return;
    }

    final int firstIdx = idxToSelect - 1; // Index of the group's title element.
    int lastIdx = nextGroupData.myLastIndexInList;

    if (firstIdx >= lastIdx || firstIdx < 0) {
      return;
    }

    if (goDown) {
      final int nextElementIdx = lastIdx + 1;

      // Check if the group is followed by "... more" element.
      if (nextElementIdx < listSize) {
        final Object nextElement = listModel.get(nextElementIdx);

        if (nextElement instanceof MoreElement) {
          lastIdx = nextElementIdx;
        }
      }
    }

    scrollToCells(firstIdx, lastIdx, goDown);
    myList.setSelectedIndex(idxToSelect);
  }

  private void scrollToCells(int firstCellIdx, int lastCellIdx, boolean preferBottom) {
    final JViewport viewport = myListScrollPane.getViewport();
    final Rectangle cellsRect = myList.getCellBounds(firstCellIdx, lastCellIdx);
    final Rectangle viewRect = viewport.getViewRect();
    final int cellsTop = cellsRect.y;
    final int viewTop = viewRect.y;

    if (preferBottom) {
      final int cellsHeight = cellsRect.height;
      final int cellsBottom = cellsTop + cellsHeight;
      final int viewHeight = viewRect.height;
      final int viewBottom = viewTop + viewHeight;

      if (cellsBottom > viewBottom) {
        final Point viewportPosition = myViewportPosition;
        viewportPosition.x = viewRect.x;

        if (cellsHeight > viewHeight) {
          // Scroll to the top of the cell.
          viewportPosition.y = cellsTop;
        }
        else {
          // Scroll to the bottom of the cell.
          viewportPosition.y = cellsBottom - viewHeight;
        }

        viewport.setViewPosition(viewportPosition);
      }
    } else {
      if (cellsTop < viewTop) {
        final Point viewportPosition = myViewportPosition;
        viewportPosition.x = viewRect.x;
        viewportPosition.y = cellsTop;
        viewport.setViewPosition(viewportPosition);
      }
    }
  }

  private void keyPressed(KeyEvent event) {
    final int keyCode = event.getKeyCode();

    switch (keyCode) {
    case KeyEvent.VK_DOWN:
      selectNextAssembly();
      break;
    case KeyEvent.VK_UP:
      selectPreviousAssembly();
      break;
    case KeyEvent.VK_TAB:
      selectNextGroup();
      break;
    case KeyEvent.VK_ESCAPE:
      setVisible(false);
      break;
    }
  }

  private static void loadAssemblies() {
    List<Assembly> systemAssemblies = ourSystemAssemblies;
    List<Assembly> localAssemblies = ourLocalAssemblies;
    List<Assembly> recentAssemblies = ourRecentAssemblies;

    if (systemAssemblies == null) {
      systemAssemblies = loadAssemblies("SystemAssemblies.txt");
      ourSystemAssemblies = systemAssemblies;
    }

    if (localAssemblies == null) {
      localAssemblies = loadAssemblies("LocalAssemblies.txt");
      ourLocalAssemblies = localAssemblies;
    }

    if (recentAssemblies == null || recentAssemblies.isEmpty()) {
      recentAssemblies = new ArrayList<Assembly>(2);

      if (systemAssemblies != null && !systemAssemblies.isEmpty()) {
        recentAssemblies.add(systemAssemblies.get(0));
      }

      if (localAssemblies != null && !localAssemblies.isEmpty()) {
        recentAssemblies.add(localAssemblies.get(0));
      }

      ourRecentAssemblies = recentAssemblies;
    }
  }

  private static List<Assembly> loadAssemblies(String resourceFileName) {
    final InputStream stream =
        AddDotNetAssemblyDialog.class.getResourceAsStream(resourceFileName);
    assert stream != null;
    final BufferedReader reader;

    try {
      reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
    } catch (UnsupportedEncodingException exception) {
      assert false;
      return null;
    }

    final List<Assembly> assemblies = new ArrayList<Assembly>();

    try {
      String path = null;

      while ((path = reader.readLine()) != null) {
        assemblies.add(new Assembly(path));
      }
    } catch (IOException exception) {
      return null;
    }
    finally {
      try {
        reader.close();
      } catch (IOException ignored) {
      }
    }

    return assemblies;
  }

  private static Font getTitleFont() {
    return UIUtil.getLabelFont().deriveFont(UIUtil.getFontSize(UIUtil.FontSize.SMALL));
  }

  private static JBColor cloneColor(Color color) {
    final int rgb = color.getRGB();
    return new JBColor(rgb, rgb);
  }

  private final class ListSelection {
    private int myIndexInList;
    private int myGroupIndex;
    private int myIndexInGroup;

    private ListSelection() {
      invalidate();
    }

    private void invalidate() {
      myIndexInList = -1;
      myGroupIndex = -1;
      myIndexInGroup = -1;
    }

    private boolean isValid() {
      return myIndexInList >= 0 && myGroupIndex >= 0 && myIndexInGroup >= 0;
    }

    private void update(int indexInList) {
      if (indexInList < 0) {
        invalidate();
        return;
      }

      final GroupData[] groupsData = myGroupsData;
      final int groupCount = groupsData.length;

      for (int groupIdx = 0; groupIdx < groupCount; ++groupIdx) {
        final GroupData groupData = groupsData[groupIdx];
        final int firstIdx = groupData.myFirstIndexInList;

        if (indexInList >= firstIdx &&
            indexInList <= groupData.myLastIndexInList) {
          myIndexInList = indexInList;
          myGroupIndex = groupIdx;
          myIndexInGroup = indexInList - firstIdx;
          return;
        }
      }

      invalidate();
    }
  }

  private final class DialogListener extends ComponentAdapter {
    @Override
    public void componentShown(ComponentEvent event) {
      mySearchTextField.grabFocus();
      mySearchTextField.setText("");
      launchSearcher("");
    }

    @Override
    public void componentResized(ComponentEvent event) {
      final Container contentPane = getContentPane();

      if (contentPane == null) {
        return;
      }

      Dimension contentMinSize = myContentMinSize;
      final Dimension contentNewMinSize = myLayoutManager.minimumLayoutSize(contentPane);
      boolean toUpdateMinSize = false;

      if (contentMinSize == null) {
        contentMinSize = new Dimension(contentNewMinSize);
        myContentMinSize = contentMinSize;
        toUpdateMinSize = true;
      }
      else {
        if (contentMinSize.width != contentNewMinSize.width ||
            contentMinSize.height != contentNewMinSize.height) {
          contentMinSize.setSize(contentNewMinSize);
          toUpdateMinSize = true;
        }
      }

      if (toUpdateMinSize) {
        Dimension dialogMinSize = myDialogMinSize;

        if (dialogMinSize == null) {
          dialogMinSize = new Dimension(contentMinSize);
          myDialogMinSize = dialogMinSize;
        }
        else {
          dialogMinSize.setSize(contentMinSize);
        }

        // Amend for Dialog's window decoration.
        dialogMinSize.width += getWidth() - contentPane.getWidth();
        dialogMinSize.height += getHeight() - contentPane.getHeight();
        setMinimumSize(dialogMinSize);
      }
    }
  }

  private final class SearchTextListener extends DocumentAdapter implements KeyListener {
    @Override
    public void keyPressed(KeyEvent event) {
      AddDotNetAssemblyDialog.this.keyPressed(event);
    }

    @Override
    public void keyReleased(KeyEvent event) {
    }

    @Override
    public void keyTyped(KeyEvent event) {
    }

    @Override
    protected void textChanged(DocumentEvent event) {
      launchSearcher(mySearchTextField.getText());
    }
  }

  private final class ListListener extends MouseAdapter implements FocusListener,
                                                                   ListSelectionListener {
    @Override
    public void mouseClicked(MouseEvent event) {
      if (event.getButton() == MouseEvent.BUTTON1) {
        final Point point = event.getPoint();
        final JList list = myList;
        final int listIdx = myList.locationToIndex(point);

        if (listIdx < 0) {
          return;
        }

        final DefaultListModel listModel = myListModel;

        if (listIdx >= listModel.size()) {
          return;
        }

        final Object clickedElement = listModel.get(listIdx);

        if (clickedElement instanceof MoreElement &&
            list.getCellBounds(listIdx, listIdx).contains(point)) {
          for (final GroupData groupData : myGroupsData) {
            if (clickedElement == groupData.myMoreElement) {
              groupData.myViewSize += VIEW_SIZE_INCREMENT;
            }
          }

          updateList();
        }
      }
    }

    @Override
    public void focusGained(FocusEvent event) {
      // Keep the search text control in focus to let user keep typing.
      mySearchTextField.grabFocus();
    }

    @Override
    public void focusLost(FocusEvent event) {
    }

    @Override
    public void valueChanged(ListSelectionEvent event) {
      final JList list = myList;
      final DefaultListModel listModel = myListModel;
      final ListSelection listSelection = myListSelection;
      final int firstIdx = event.getFirstIndex();
      final int lastIdx = event.getLastIndex();

      if (firstIdx <= lastIdx && firstIdx >= 0 && lastIdx < listModel.size()) {
        final int selectedIdx;

        if (list.isSelectedIndex(firstIdx)) {
          selectedIdx = firstIdx;
        } else if (list.isSelectedIndex(lastIdx)) {
          selectedIdx = lastIdx;
        } else {
          return;
        }

        final Object element = listModel.get(selectedIdx);

        if (element instanceof FoundAssembly) {
          listSelection.update(selectedIdx);
          myPropertyPane.setAssembly(((FoundAssembly)element).myAssembly);
        }
        else {
          // Deny selection of non-assembly elements (like e.g. Group Title).
          restoreListSelection();
        }
      }
    }
  }

  private final class BrowseButtonListener implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent event) {
      FileFilter fileFilter = myFileFilter;

      if (fileFilter == null) {
        fileFilter = new FileNameExtensionFilter(".Net Assemblies", "dll");
        myFileFilter = fileFilter;
      }

      final JFileChooser fileChooser = new JFileChooser();
      fileChooser.setFileFilter(fileFilter);
      fileChooser.showOpenDialog(AddDotNetAssemblyDialog.this);
      mySearchTextField.grabFocus();
    }
  }

  private final class DialogLayoutManager implements LayoutManager {
    private Dimension myDimension = null;

    @Override
    public void addLayoutComponent(String name, Component component) {
    }

    @Override
    public void removeLayoutComponent(Component component) {
    }

    @Override
    public void layoutContainer(Container container) {
      final int containerWidth = container.getWidth();
      final int containerHeight = container.getHeight();
      final Component searchTextField = mySearchTextField;
      final Component browseButton = myBrowseButton;
      final Component listScrollPane = myListScrollPane;
      final Component propertyPane = myPropertyPane;
      final Dimension searchTextFieldPrefSize = searchTextField.getPreferredSize();
      final Dimension browseButtonPrefSize = browseButton.getPreferredSize();
      final Dimension propertyPanePrefSize = propertyPane.getPreferredSize();
      final int searchTextFieldHeight = searchTextFieldPrefSize.height;
      final int browseButtonWidth = browseButtonPrefSize.width;
      final int browseButtonHeight = browseButtonPrefSize.height;
      final int listPrefHeight = searchTextFieldHeight * 15;
      final int firstRowHeight = Math.max(searchTextFieldHeight, browseButtonHeight);
      int secondRowHeight = Math.max(listPrefHeight, propertyPanePrefSize.height);
      int firstColWidth = searchTextFieldPrefSize.width;
      int secondColWidth = Math.max(browseButtonWidth, propertyPanePrefSize.width);

      final int gap = searchTextFieldHeight >> 1;
      final int gapAmendment = gap * 3;
      final int excessiveHeight = containerHeight - firstRowHeight - secondRowHeight - gapAmendment;
      final int excessiveWidth = containerWidth - firstColWidth - secondColWidth - gapAmendment;

      if (excessiveHeight > 0) {
        secondRowHeight += excessiveHeight;
      }

      if (excessiveWidth > 0) {
        // Spread the excessive width evenly between the two columns in out layout.
        final int halfExcessiveWidth = excessiveWidth >> 1;
        firstColWidth += halfExcessiveWidth + (excessiveWidth & 1);
        secondColWidth += halfExcessiveWidth;
      }

      final int secondRowTop = firstRowHeight + (gap << 1);
      final int secondColLeft = firstColWidth + (gap << 1);
      final int firstRowMedianY = gap + (firstRowHeight >> 1);

      searchTextField.setLocation(gap, firstRowMedianY - (searchTextFieldHeight >> 1));
      searchTextField.setSize(firstColWidth, searchTextFieldHeight);

      browseButton.setLocation(secondColLeft, firstRowMedianY - (browseButtonHeight >> 1));
      browseButton.setSize(browseButtonPrefSize);

      listScrollPane.setLocation(gap, secondRowTop);
      listScrollPane.setSize(firstColWidth, secondRowHeight);

      propertyPane.setLocation(secondColLeft, secondRowTop);
      propertyPane.setSize(secondColWidth, secondRowHeight);
    }

    @Override
    public Dimension minimumLayoutSize(Container container) {
      return preferredLayoutSize(container);
    }

    @Override
    public Dimension preferredLayoutSize(Container container) {
      final Dimension searchTextFieldPrefSize = mySearchTextField.getPreferredSize();
      final Dimension browseButtonPrefSize = myBrowseButton.getPreferredSize();
      final Dimension propertyPanePrefSize = myPropertyPane.getPreferredSize();
      final int searchTextFieldHeight = searchTextFieldPrefSize.height;
      final int browseButtonHeight = browseButtonPrefSize.height;
      final int listPrefHeight = searchTextFieldHeight * 15;
      final int firstRowHeight = Math.max(searchTextFieldHeight, browseButtonHeight);
      final int secondRowHeight = Math.max(listPrefHeight, propertyPanePrefSize.height);
      final int firstColWidth = searchTextFieldPrefSize.width;
      final int secondColWidth = Math.max(browseButtonPrefSize.width, propertyPanePrefSize.width);
      final int gap = searchTextFieldHeight >> 1;

      Dimension dimension = myDimension;

      if (dimension == null) {
        dimension = new Dimension();
        myDimension = dimension;
      }

      final int gapAmendment = gap * 3;
      dimension.width = firstColWidth + secondColWidth + gapAmendment;
      dimension.height = firstRowHeight + secondRowHeight + gapAmendment;
      return dimension;
    }
  }

  private final class ListCellRenderer extends ColoredListCellRenderer {
    @Override
    public Component getListCellRendererComponent(JList list,
                                                  Object value,
                                                  int index,
                                                  boolean selected,
                                                  boolean hasFocus) {
      if (value instanceof FoundAssembly) {
        return super.getListCellRendererComponent(list, value, index, selected, hasFocus);
      }

      if (value instanceof TitleElement) {
        for (final GroupData groupData : myGroupsData) {
          if (value == groupData.myTitleElement) {
            return groupData.myTitleComponent;
          }
        }
      } else if (value instanceof  MoreElement) {
        for (final GroupData groupData : myGroupsData) {
          if (value == groupData.myMoreElement) {
            return groupData.myMoreComponent;
          }
        }
      }

      assert false;
      return null;
    }

    @Override
    protected void customizeCellRenderer(JList list,
                                         Object value,
                                         int index,
                                         boolean selected,
                                         boolean hasFocus) {
      final FoundAssembly foundAssembly = (FoundAssembly)value;
      final String assemblyName = foundAssembly.myAssembly.myName;
      final int matchOffset = foundAssembly.mySearchMatchOffset;

      if (matchOffset < 0) {
        append(assemblyName);
      } else {
        if (matchOffset > 0) {
          append(assemblyName.substring(0, matchOffset), SimpleTextAttributes.REGULAR_ATTRIBUTES);
        }

        final int matchEndIdx = matchOffset + mySearchQuery.length();
        append(assemblyName.substring(matchOffset, matchEndIdx), SEARCH_MATCH_ATTRIBUTES);

        if (matchEndIdx < assemblyName.length()) {
          append(assemblyName.substring(matchEndIdx), SimpleTextAttributes.REGULAR_ATTRIBUTES);
        }
      }
    }
  }

  private static final class Assembly implements Comparable<Assembly> {
    private static final String HARDCODED_VERSION = "4.0.0";
    private static final String HARDCODED_MVID = "0D6B30FC-75AB-47CC-9690-D19AF0BF6864";
    private static final String HARDCODED_PUBLIC_KEY = "b77a5c561934e089";
    private static final String[] HARDCODED_REFERENCES = {
      "mscorlib.dll",
      "System.Xml.dll",
      "System.Core.dll"
    };

    private static final String PLATFORM_BASE = ".Net Framework ";
    private static final String DEFAULT_PLATFORM = ".Net Framework v4.6";
    private static final int PLATFORM_BASE_LENGTH = PLATFORM_BASE.length();

    private static final int LEX_STATE_INITIAL = 0;
    private static final int LEX_STATE_BACK_SLASH = 1;
    private static final int LEX_STATE_V = 2;
    private static final int LEX_STATE_V_NUM = 3;
    private static final int LEX_STATE_V_NUM_DOT = 4;

    private final String myName;
    private String myNameLowerCased;
    private final String myPath;
    private final String myPlatform;

    @Override
    public int compareTo(Assembly x) {
      return myName.compareTo(x.myName);
    }

    private Assembly(String path) {
      final int pathLen = path.length();
      final StringBuilder platform = new StringBuilder(PLATFORM_BASE);
      boolean platformDetected = false;
      int lastSlashIdx = -1;
      int lexState = LEX_STATE_INITIAL;

      for (int i = 0; i < pathLen; ++i) {
        final char ch = path.charAt(i);

        if (ch == '\\') {
          lastSlashIdx = i;
        }

        if (platformDetected) {
          continue;
        }

        switch (lexState) {
        case LEX_STATE_INITIAL:
          if (ch == '\\') {
            lexState = LEX_STATE_BACK_SLASH;
          }
          break;

        case LEX_STATE_BACK_SLASH:
          if (ch == 'v') {
            lexState = LEX_STATE_V;
            platform.append(ch);
          }
          else {
            lexState = LEX_STATE_INITIAL;
          }
          break;

        case LEX_STATE_V:
        case LEX_STATE_V_NUM_DOT:
          if (Character.isDigit(ch)) {
            lexState = LEX_STATE_V_NUM;
            platform.append(ch);
          }
          else {
            lexState = LEX_STATE_INITIAL;
            platform.setLength(PLATFORM_BASE_LENGTH);
          }
          break;

        case LEX_STATE_V_NUM:
          switch (ch) {
          case '.':
            lexState = LEX_STATE_V_NUM_DOT;
            platform.append(ch);
            break;
          case '\\':
            platformDetected = true;
            break;
          default:
            if (Character.isDigit(ch)) {
              platform.append(ch);
            }
            else {
              lexState = LEX_STATE_INITIAL;
              platform.setLength(PLATFORM_BASE_LENGTH);
            }
          }
          break;
        }
      }

      myName = lastSlashIdx >= 0 ? path.substring(lastSlashIdx + 1) : path;
      myNameLowerCased = null;
      myPath = path;
      myPlatform = platformDetected ? platform.toString() : DEFAULT_PLATFORM;
    }

    private String getName() {
      return myName;
    }

    private String getVersion() {
      return HARDCODED_VERSION;
    }

    private String getMvid() {
      return HARDCODED_MVID;
    }

    private String getPublicKey() {
      return HARDCODED_PUBLIC_KEY;
    }

    private String getPath() {
      return myPath;
    }

    private String getPlatform() {
      return myPlatform;
    }

    private String[] getReferences() {
      return HARDCODED_REFERENCES;
    }
  }

  private static final class FoundAssembly {
    private Assembly myAssembly;
    private int mySearchMatchOffset;

    private FoundAssembly() {
      this(null);
    }

    private FoundAssembly(Assembly assembly) {
      myAssembly = assembly;
      mySearchMatchOffset = -1;
    }
  }

  private final class Searcher implements Runnable {
    private final String mySearchQuery;
    private volatile boolean myIsCancelled;

    private Searcher(String searchQuery) {
      mySearchQuery = searchQuery.toLowerCase();
      myIsCancelled = false;
    }

    @Override
    public void run() {
      final Lock lock = myFoundAssembliesLock;

      if (myIsCancelled) {
        return;
      }

      lock.lock();

      try {
        for (final GroupData groupData : myGroupsData) {
          final int count = search(groupData.myAssemblies, groupData.myFoundAssemblies);

          if (count < 0) {
            // The task has been cancelled.
            return;
          }

          groupData.myFoundAssembliesCount = count;
        }
      } finally {
        lock.unlock();
      }

      SwingUtilities.invokeLater(myListUpdater);
    }

    private void cancel() {
      myIsCancelled = true;
    }

    private int search(Iterable<Assembly> assemblies,
                       List<FoundAssembly> foundAssemblies) {
      final String searchQuery = mySearchQuery;
      final boolean isEmptyQuery = searchQuery.isEmpty();
      final int capacity = foundAssemblies.size();
      int idx = 0;
      int matchOffset = -1;

      for (final Assembly assembly : assemblies) {
        if (myIsCancelled) {
          return -1;
        }

        if (!isEmptyQuery) {
          String assemblyNameLowerCased = assembly.myNameLowerCased;

          if (assemblyNameLowerCased == null) {
            assemblyNameLowerCased = assembly.myName.toLowerCase();
            assembly.myNameLowerCased = assemblyNameLowerCased;
          }

          matchOffset = assemblyNameLowerCased.indexOf(searchQuery);

          if (matchOffset < 0) {
            continue;
          }
        }

        final FoundAssembly foundAssembly;

        if (idx < capacity) {
          foundAssembly = foundAssemblies.get(idx);
        } else {
          foundAssembly = new FoundAssembly();
          foundAssemblies.add(foundAssembly);
        }

        foundAssembly.myAssembly = assembly;
        foundAssembly.mySearchMatchOffset = matchOffset;
        ++idx;
      }

      return idx;
    }
  }

  private final class ListUpdater implements Runnable {
    @Override
    public void run() {
      for (final GroupData groupData : myGroupsData) {
        groupData.myViewSize = INITIAL_VIEW_SIZE;
      }

      myListSelection.invalidate();
      updateList();
    }
  }

  private enum Group {
    RECENT("Recent"),
    SYSTEM(".Net Framework"),
    LOCAL("Local");

    Group(String title) {
      myTitle = title;
    }

    private final String myTitle;
  }

  private static final class GroupData {
    private final List<Assembly> myAssemblies;
    private final Component myTitleComponent;
    private final Component myMoreComponent;
    private final TitleElement myTitleElement;
    private final MoreElement myMoreElement;
    private List<FoundAssembly> myFoundAssemblies;
    private int myFoundAssembliesCount;
    private int myViewSize;
    private int myFirstIndexInList;
    private int myLastIndexInList;

    private GroupData(Group group) {
      myTitleComponent = new TitleComponent(group.myTitle);
      myMoreComponent = new MoreComponent();

      switch (group) {
      case RECENT:
        myAssemblies = ourRecentAssemblies;
        myTitleElement = TitleElement.RECENT;
        myMoreElement = MoreElement.RECENT;
        break;
      case SYSTEM:
        myAssemblies = ourSystemAssemblies;
        myTitleElement = TitleElement.SYSTEM;
        myMoreElement = MoreElement.SYSTEM;
        break;
      case LOCAL:
        myAssemblies = ourLocalAssemblies;
        myTitleElement = TitleElement.LOCAL;
        myMoreElement = MoreElement.LOCAL;
        break;
      default:
        assert false;
        myAssemblies = Collections.emptyList();
        myTitleElement = null;
        myMoreElement = null;
      }

      myFoundAssemblies = new ArrayList<FoundAssembly>(myAssemblies.size());
      myViewSize = INITIAL_VIEW_SIZE;
      myFirstIndexInList = -1;
      myLastIndexInList = -1;
    }
  }

  private static final class TitleElement {
    private static final TitleElement RECENT = new TitleElement(Group.RECENT);
    private static final TitleElement SYSTEM = new TitleElement(Group.SYSTEM);
    private static final TitleElement LOCAL = new TitleElement(Group.LOCAL);

    private final Group myGroup;

    private TitleElement(Group group) {
      myGroup = group;
    }
  }

  private static final class MoreElement {
    private static final MoreElement RECENT = new MoreElement(Group.RECENT);
    private static final MoreElement SYSTEM = new MoreElement(Group.SYSTEM);
    private static final MoreElement LOCAL = new MoreElement(Group.LOCAL);

    private final Group myGroup;

    private MoreElement(Group group) {
      myGroup = group;
    }
  }

  private static final class TitleComponent extends JPanel {
    private final Component myLabel;

    private TitleComponent(String text) {
      super(new BorderLayout());
      final Component label = new JLabel(" " + text + " ");
      label.setFont(getTitleFont());
      final Component separator = new SeparatorComponent(label.getPreferredSize().height >> 1,
                                                         new JBColor(Gray._220, Gray._80), null);
      add(label, BorderLayout.WEST);
      add(separator, BorderLayout.CENTER);
      myLabel = label;
      applyLookAndFeel(false);
      UIManager.addPropertyChangeListener(new UIPropertyChangeListener());
    }

    private void applyLookAndFeel(boolean updateFont) {
      final Component label = myLabel;
      final Color bgColor = cloneColor(UIUtil.getListBackground());
      final Color fgColor = cloneColor(UIUtil.getLabelDisabledForeground());
      setBackground(bgColor);
      label.setBackground(bgColor);
      label.setForeground(fgColor);

      if (updateFont) {
        label.setFont(getTitleFont());
      }
    }

    private final class UIPropertyChangeListener implements PropertyChangeListener {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        applyLookAndFeel(true);
      }
    }
  }

  private static final class MoreComponent extends JPanel {
    private final Component myLabel;

    private MoreComponent() {
      super(new BorderLayout());
      final Component label = new JLabel(" ... more");
      add(label);
      myLabel = label;
      applyLookAndFeel();
      UIManager.addPropertyChangeListener(new UIPropertyChangeListener());
    }

    private void applyLookAndFeel() {
      final Component label = myLabel;
      final Color bgColor = cloneColor(UIUtil.getListBackground());
      final Color fgColor = cloneColor(UIUtil.getLabelDisabledForeground());
      setBackground(bgColor);
      label.setBackground(bgColor);
      label.setForeground(fgColor);
      label.setFont(getTitleFont());
    }

    private final class UIPropertyChangeListener implements PropertyChangeListener {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        applyLookAndFeel();
      }
    }
  }

  private static final class PropertyPane extends JPanel {
    private final LayoutManager myLayoutManager;
    private final JTextComponent myNameText;
    private final JTextComponent myVersionText;
    private final JTextComponent myMvidText;
    private final JTextComponent myPublicKeyText;
    private final JTextComponent myPathText;
    private final JTextComponent myPlatformText;
    private final JTextArea myReferencesText;
    private final Component[] myComponents;
    private Dimension myDimension;
    private Stroke myBorderStroke;
    private Color myBorderColor;
    private Font myFont;
    private int myFontWidth;
    private int myFontHeight;

    @Override
    public Dimension getPreferredSize() {
      return myLayoutManager.preferredLayoutSize(this);
    }

    @Override
    public Dimension getMinimumSize() {
      return myLayoutManager.minimumLayoutSize(this);
    }

    @Override
    protected void paintComponent(Graphics graphics) {
      super.paintComponent(graphics);

      final int width = getWidth();
      final int height = getHeight();

      if (width > 1 && height > 1) {
        Stroke borderStroke = myBorderStroke;
        Color borderColor = myBorderColor;

        if (borderStroke == null) {
          borderStroke = new BasicStroke(1.0f);
          myBorderStroke = borderStroke;
        }

        if (borderColor == null) {
          borderColor = cloneColor(JBColor.border());
          myBorderColor = borderColor;
        }

        graphics.setColor(borderColor);
        ((Graphics2D)graphics).setStroke(borderStroke);
        graphics.drawRect(0, 0, width - 1, height - 1);
      }
    }

    private PropertyPane() {
      final LayoutManager layoutManager = new MyLayoutManager();
      setLayout(layoutManager);

      final Component nameLabel = new JLabel("Name:");
      final JTextComponent nameText = createTextComponent();
      final Component versionLabel = new JLabel("Version:");
      final JTextComponent versionText = createTextComponent();
      final Component mvidLabel = new JLabel("MVID:");
      final JTextComponent mvidText = createTextComponent();
      final Component publicKeyLabel = new JLabel("PublicKey Token:");
      final JTextComponent publicKeyText = createTextComponent();
      final Component pathLabel = new JLabel("Location:");
      final JTextArea pathText = createTextComponent();
      pathText.setLineWrap(true);
      pathText.setWrapStyleWord(true);
      final Component platformLabel = new JLabel("Platform:");
      final JTextComponent platformText = createTextComponent();
      final Component referencesLabel = new JLabel("References:");
      final JTextArea referencesText = createTextComponent();

      add(nameLabel);
      add(nameText);
      add(versionLabel);
      add(versionText);
      add(mvidLabel);
      add(mvidText);
      add(publicKeyLabel);
      add(publicKeyText);
      add(pathLabel);
      add(pathText);
      add(platformLabel);
      add(platformText);
      add(referencesLabel);
      add(referencesText);

      myLayoutManager = layoutManager;
      myNameText = nameText;
      myVersionText = versionText;
      myMvidText = mvidText;
      myPublicKeyText = publicKeyText;
      myPathText = pathText;
      myPlatformText = platformText;
      myReferencesText = referencesText;
      myComponents = new Component[] {
          nameLabel,
          nameText,
          versionLabel,
          versionText,
          mvidLabel,
          mvidText,
          publicKeyLabel,
          publicKeyText,
          pathLabel,
          pathText,
          platformLabel,
          platformText,
          referencesLabel,
          referencesText
      };
      myDimension = null;
      myBorderStroke = null;
      myBorderColor = null;
      myFont = null;
      myFontWidth = 1;
      myFontHeight = 1;

      applyLookAndFeel();
      UIManager.addPropertyChangeListener(new UIPropertyChangeListener());
    }

    private void setAssembly(Assembly assembly) {
      if (assembly == null) {
        myNameText.setText("");
        myVersionText.setText("");
        myMvidText.setText("");
        myPublicKeyText.setText("");
        myPathText.setText("");
        myPlatformText.setText("");
        myReferencesText.setText("");
      } else {
        myNameText.setText(assembly.getName());
        myVersionText.setText(assembly.getVersion());
        myMvidText.setText(assembly.getMvid());
        myPublicKeyText.setText(assembly.getPublicKey());
        myPathText.setText(assembly.getPath());
        myPlatformText.setText(assembly.getPlatform());

        final String[] references = assembly.getReferences();
        final JTextArea referencesText = myReferencesText;

        referencesText.setRows(references.length);
        final StringBuilder strBld = new StringBuilder();

        for (final String reference : references) {
          if (strBld.length() > 0) {
            strBld.append("\n");
          }

          strBld.append(reference);
        }

        referencesText.setText(strBld.toString());
      }
    }

    private void applyLookAndFeel() {
      final Font font = getUIFont();

      final Color bgColor = cloneColor(UIUtil.getPanelBackground());
      final Color fgColor = cloneColor(UIUtil.getLabelForeground());

      for (final Component component : myComponents) {
        component.setBackground(bgColor);
        component.setForeground(fgColor);
        component.setFont(font);
      }

      // Reset the border color, so it will be set according to the new L&F when
      // the component gets painted next time.
      myBorderColor = null;
      myFont = font;
    }

    private void updateFont() {
      Font font = myFont;

      if (font == null) {
        font = getUIFont();
        myFont = font;
      }

      final FontMetrics fontMetrics = getFontMetrics(font);
      final int fontHeight = fontMetrics.getHeight();

      if (fontHeight == myFontHeight) {
        return;
      }

      int fontWidth = 1;

      for (char ch = 'A'; ch <= 'Z'; ++ch) {
        final int charWidth = fontMetrics.charWidth(ch);

        if (charWidth > fontWidth) {
          fontWidth = charWidth;
        }
      }

      for (char ch = '0'; ch <= '9'; ++ch) {
        final int charWidth = fontMetrics.charWidth(ch);

        if (charWidth > fontWidth) {
          fontWidth = charWidth;
        }
      }

      final int charWidth = fontMetrics.charWidth('-');

      if (charWidth > fontWidth) {
        fontWidth = charWidth;
      }

      myFontWidth = fontWidth;
      myFontHeight = fontHeight;
    }

    private static JTextArea createTextComponent() {
      final JTextArea component = new JTextArea();
      component.setEditable(false);
      component.setBorder(null);
      return component;
    }

    private static Font getUIFont() {
      return UIUtil.getLabelFont();
    }

    private final class MyLayoutManager implements LayoutManager {
      @Override
      public void addLayoutComponent(String name, Component component) {
      }

      @Override
      public void removeLayoutComponent(Component component) {
      }

      @Override
      public Dimension preferredLayoutSize(Container container) {
        Dimension dimension = myDimension;

        if (dimension == null) {
          dimension = new Dimension();
          myDimension = dimension;
        }

        updateFont();
        final int fontWidth = myFontWidth;
        dimension.width = fontWidth * 37 + 2;
        dimension.height = myFontHeight * 28 + fontWidth + 2;

        return dimension;
      }

      @Override
      public Dimension minimumLayoutSize(Container container) {
        return preferredLayoutSize(container);
      }

      @Override
      public void layoutContainer(Container container) {
        updateFont();
        final int fontWidth = myFontWidth;
        final int fontHeight = myFontHeight;
        final int hMargin = fontWidth;
        final int vMargin = fontWidth >> 1;
        int top = vMargin;
        int idx = 0;

        for (final Component component : myComponents) {
          Dimension prefSize = component.getPreferredSize();
          component.setLocation(hMargin, top);

          if (component instanceof JTextArea &&
              ((JTextArea)component).getLineWrap()) {
            component.setSize(fontWidth * 36, prefSize.height);
          } else {
            component.setSize(prefSize);
          }

          top += prefSize.height;

          if ((idx & 1) == 1) {
            top += fontHeight;
          }

          ++idx;
        }
      }
    }

    private final class UIPropertyChangeListener implements PropertyChangeListener {
      @Override
      public void propertyChange(PropertyChangeEvent event) {
        applyLookAndFeel();
      }
    }
  }
}
