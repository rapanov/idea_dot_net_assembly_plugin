/**
 * Copyright (C) 2016, Roman Panov (roman.a.panov@gmail.com).
 */

package org.panovrom.intellij;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Window;
import javax.swing.JDialog;

public class AddDotNetAssemblyAction extends AnAction {
  private JDialog myDialog = null;

  @Override
  public void actionPerformed(AnActionEvent event) {
    assert EventQueue.isDispatchThread();
    JDialog dialog = myDialog;

    if (dialog == null) {
      final Frame activeFrame = getActiveFrame();

      if (activeFrame == null) {
        return;
      }

      dialog = new AddDotNetAssemblyDialog(activeFrame);
      myDialog = dialog;
      dialog.pack();
    }

    dialog.setVisible(true);
  }

  private static Frame getActiveFrame() {
    final Window[] windows = Window.getWindows();

    for (final Window window : windows) {
      if (window instanceof Frame && window.isActive()) {
        return (Frame) window;
      }
    }

    return null;
  }

}
